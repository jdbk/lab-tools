window.onload = function() {
    const container = document.getElementById('tiles');
    for (let i = 0; i < 100; i++) {
        const img = document.createElement('img');
        img.src = `/img/image-${i}.png?nocache=${new Date().getTime()}`;
        img.className = 'tile';
        container.appendChild(img);
    }
};
