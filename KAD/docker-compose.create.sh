#!/bin/bash

# Define the number of instances
num_instances=3

for (( i=1; i<=num_instances; i++ ))
do
  # Generate a unique port number for redis and app
  redis_port="637${i}"
  app_port="808${i}"

  # Use 'envsubst' to replace the variables in the template
  app_nr=$i redis_port=$redis_port app_port=$app_port envsubst < docker-compose.template.yml > "docker-compose-instance-${i}.yml"

  # Bring up the instance with a unique project name
  docker-compose -p "instance${i}" -f "docker-compose-instance-${i}.yml" up -d
done
